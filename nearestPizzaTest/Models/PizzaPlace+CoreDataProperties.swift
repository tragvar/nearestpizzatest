//
//  PizzaPlace+CoreDataProperties.swift
//  
//
//  Created by Illya on 4/11/17.
//
//

import Foundation
import CoreData
import SwiftyJSON

extension PizzaPlace {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<PizzaPlace> {
        return NSFetchRequest<PizzaPlace>(entityName: "PizzaPlace")
    }

    @NSManaged public var distance: Int32
    @NSManaged public var identifier: String?
    @NSManaged public var name: String?
    @NSManaged public var pizzaPlaceDetails: PizzaPlaceDetails?

    func updateWith(json: JSON) {
        self.name = json["name"].stringValue
        self.identifier = json["id"].stringValue
        self.distance = Int32(json["location"]["distance"].intValue)
    }
    
    class func findOrCreatePlaceWith(identifier: String, context: NSManagedObjectContext) -> PizzaPlace {
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: self.className())
        fetchRequest.predicate = NSPredicate(format: "identifier = %@", identifier)
        var results: [PizzaPlace]?
        do {
            //Succes
            results = try context.fetch(fetchRequest) as? [PizzaPlace]
            if let obj = results?.last {
                return obj
            }
        } catch let error as NSError {
            //Error
            print("Error \(error)")
        }
        let pizzaPlace = NSEntityDescription.insertNewObject(forEntityName: self.className(), into: context) as! PizzaPlace
        return pizzaPlace
    }
}
