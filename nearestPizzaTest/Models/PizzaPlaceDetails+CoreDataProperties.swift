//
//  PizzaPlaceDetails+CoreDataProperties.swift
//  
//
//  Created by Illya on 4/11/17.
//
//

import Foundation
import CoreData
import SwiftyJSON

extension PizzaPlaceDetails {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<PizzaPlaceDetails> {
        return NSFetchRequest<PizzaPlaceDetails>(entityName: "PizzaPlaceDetails")
    }

    @NSManaged public var categories: String?
    @NSManaged public var contact: String?
    @NSManaged public var location: String?
    @NSManaged public var name: String?
    @NSManaged public var identifier: String?
    @NSManaged public var pizzaPlace: PizzaPlace?

    func updateWith(json: JSON) {
        self.name = json["name"].stringValue
        self.identifier = json["id"].stringValue
        self.contact = json["contact"]["formattedPhone"].stringValue
        
        var tempCat = ""
        json["categories"].arrayValue.forEach { (category) in
            tempCat = tempCat + category["pluralName"].stringValue + "\n"
        }
        self.categories = tempCat
        
        var tempLocation = ""
        json["location"]["formattedAddress"].arrayValue.forEach { (location) in
            tempLocation = tempLocation + location.stringValue + "\n"
        }
        self.location = tempLocation
    }
    
    class func findOrCreatePlaceDetailsWith(identifier: String, context: NSManagedObjectContext) -> PizzaPlaceDetails {
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: self.className())
        fetchRequest.predicate = NSPredicate(format: "identifier = %@", identifier)
        var results: [PizzaPlaceDetails]?
        do {
            //Succes
            results = try context.fetch(fetchRequest) as? [PizzaPlaceDetails]
            if let obj = results?.last {
                return obj
            }
        } catch let error as NSError {
            //Error
            print("Error \(error)")
        }
        let pizzaPlaceDetails = NSEntityDescription.insertNewObject(forEntityName: self.className(), into: context) as! PizzaPlaceDetails
        return pizzaPlaceDetails
    }

}
