//
//  Constats.swift
//  nearestPizzaTest
//
//  Created by Illya on 4/10/17.
//  Copyright © 2017 Tragvar. All rights reserved.
//

import Foundation

struct Constants {
    
    static let kFoursquareToken = "OTA1KBKDZXJ1SINSDZGJ0YL44LY0JUSNMDNNSZSYG0YSNAH5"
    static let kFoursquareSearchDomain = "https://api.foursquare.com/v2/venues/search"
    static let kPizzaURLStr = kFoursquareSearchDomain + "?ll=%@,%@&limit=%ld&query=pizza&oauth_token=\(kFoursquareToken)&v=20151119"
    
    struct NotificationKey {
        static let DidChangeLocationStatus = "kDidChangeLocationStatus"
        static let DidReceiveLocationFix = "kDidReceiveLocationFix"
    }
    
}
