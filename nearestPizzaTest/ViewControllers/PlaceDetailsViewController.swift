//
//  PlaceDetailsViewController.swift
//  nearestPizzaTest
//
//  Created by Illya on 4/10/17.
//  Copyright © 2017 Tragvar. All rights reserved.
//

import UIKit

class PlaceDetailsViewController: UIViewController {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var contactLabel: UILabel!
    @IBOutlet weak var locationTextView: UITextView!
    @IBOutlet weak var categoriesTextView: UITextView!
    
    var details = PizzaPlaceDetails()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        nameLabel.text = details.name
        contactLabel.text = details.contact
        locationTextView.text = details.location
        categoriesTextView.text = details.categories
    }

}
