//
//  PlacesListViewController.swift
//  nearestPizzaTest
//
//  Created by Illya on 4/10/17.
//  Copyright © 2017 Tragvar. All rights reserved.
//

import UIKit
import CoreLocation
import KVNProgress
import Async
import SwiftyJSON
import CoreData

class PlacesListViewController: UIViewController {

    @IBOutlet weak var ibTableView: UITableView!
    var dataSource = [PizzaPlace]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(PlacesListViewController.didReceiveFixLocation(_:)), name: NSNotification.Name(rawValue: Constants.NotificationKey.DidReceiveLocationFix), object: nil)

        self.ibTableView.register(PlacesListTableViewCell.nib(), forCellReuseIdentifier: PlacesListTableViewCell.className())
        updateData()
    }
    
    private func updateData() {
        let fetchRequest:NSFetchRequest<PizzaPlace> = PizzaPlace.fetchRequest()
        
        do {
            let searchResults = try CoreDataStack.getMainContext().fetch(fetchRequest)
            self.dataSource = searchResults
            self.dataSource.sort(by: { $0.distance < $1.distance })
            self.ibTableView.reloadData()
        } catch {
            print("Error: \(error.localizedDescription)")
        }

    }

    // MARK: - Notification Methods
    @objc private func didReceiveFixLocation(_ notification: Notification) {
        KVNProgress.show()
        
        if let locationManager = appDelegate.locationManager {
            
            let pizzaURLStr = String(format: Constants.kPizzaURLStr, "\(locationManager.lastLocationFix.coordinate.latitude)", "\(locationManager.lastLocationFix.coordinate.longitude)", 20)
            
            let downloadTask: URLSessionDataTask = URLSession.shared.dataTask(with: URL(string: pizzaURLStr)!, completionHandler: { [weak self] (data, response, error) -> Void in
                
                if (error != nil) {
                    KVNProgress.dismiss()
                    return
                }
                
                if let weakSelf = self, data != nil {
                    let json = JSON(data!)
                    let venues = json["response"]["venues"].arrayValue

                    let context = CoreDataStack.getMainContext()
                    venues.forEach({ (venue) in
                        let pizzaPlace = PizzaPlace.findOrCreatePlaceWith(identifier: venue["id"].stringValue, context: context)
                        let pizzaPlaceDetails = PizzaPlaceDetails.findOrCreatePlaceDetailsWith(identifier: venue["id"].stringValue, context: context)

                        pizzaPlace.updateWith(json: venue)
                        pizzaPlaceDetails.updateWith(json: venue)
                        pizzaPlaceDetails.pizzaPlace = pizzaPlace
                        pizzaPlace.pizzaPlaceDetails = pizzaPlaceDetails
                        
                        CoreDataStack.saveContext()
                        DispatchQueue.main.async(execute: {() in
                            weakSelf.updateData()
                        })
                    })
                    KVNProgress.dismiss()
                    return
                } else {
                    KVNProgress.dismiss()
                }
            })
            downloadTask.resume()
        }
    }

    
}

extension PlacesListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 	PlacesListTableViewCell.height
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : PlacesListTableViewCell = tableView.dequeueReusableCell(withIdentifier: PlacesListTableViewCell.className())! as! PlacesListTableViewCell
        cell.configWith(place: dataSource[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let placeDetailsVC = storyboard.instantiateViewController(withIdentifier: PlaceDetailsViewController.className()) as? PlaceDetailsViewController
        placeDetailsVC?.details = dataSource[indexPath.row].pizzaPlaceDetails!
        self.navigationController?.pushViewController(placeDetailsVC!, animated: true)
    }

    
}



