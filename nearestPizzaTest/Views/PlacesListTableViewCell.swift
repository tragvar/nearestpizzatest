//
//  PlacesListTableViewCell.swift
//  nearestPizzaTest
//
//  Created by Illya on 4/10/17.
//  Copyright © 2017 Tragvar. All rights reserved.
//

import UIKit

class PlacesListTableViewCell: UITableViewCell {

    static let height : CGFloat = 64

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func configWith(place: PizzaPlace) {
        self.titleLabel.text = place.name
        self.distanceLabel.text = "\(place.distance)m."
    }
    
}
