//
//  UIView+nearestPizzaTest.swift
//  nearestPizzaTest
//
//  Created by Illya on 4/10/17.
//  Copyright © 2017 Tragvar. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    class func nib() -> UINib {
        return UINib(nibName: self.className(), bundle: nil)
    }
}
