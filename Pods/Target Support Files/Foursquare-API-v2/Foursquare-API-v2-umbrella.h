#ifdef __OBJC__
#import <UIKit/UIKit.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif

#import "Foursquare2.h"
#import "FSKeychain.h"
#import "FSOAuthNoAppStore.h"
#import "FSOperation.h"
#import "FSWebLogin.h"

FOUNDATION_EXPORT double Foursquare_API_v2VersionNumber;
FOUNDATION_EXPORT const unsigned char Foursquare_API_v2VersionString[];

